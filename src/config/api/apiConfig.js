const apiConfig = {
  baseUrl: 'https://api.themoviedb.org/3/',
  apiKey: "681ef6d1eaaed5d0a07e3a9c519c1a8d",
  originalImage: (imgPath) => `https://image.tmdb.org/t/p/original/${imgPath}`,
  w500Image: (imgPath) => `https://image.tmdb.org/t/p/w500/${imgPath}`,
};

export default apiConfig