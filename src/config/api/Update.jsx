import React, { useState } from "react"
import axios from "axios"
import { Select, Option, Textarea, Input } from "@material-tailwind/react"

export const Update = () => {

    const [name, setName] = useState("");
    const [author, setAuthor] = useState("");
    const [description, setDescription] = useState("");
    const [imageUrl, setImageUrl] = useState("");
    const [dispo, setDispo] = useState(true);

    const postData = () => {
        axios.post(`https://6361039f67d3b7a0a6bb4ae9.mockapi.io/api/book`, {
            name, 
            author,
            description, 
            imageUrl,
            dispo
        });
        console.log(name);
        console.log(author);
        console.log(description);    
        console.log(imageUrl);
        console.log(dispo);
           
    }
    return (
    <div className="w-72">
      <Select label="Select Version">
        <Option>Material Tailwind HTML</Option>
        <Option>Material Tailwind React</Option>
        <Option>Material Tailwind Vue</Option>
        <Option>Material Tailwind Angular</Option>
        <Option>Material Tailwind Svelte</Option>
      </Select>

      <div className="w-72">
        <Input label="Input With Icon" icon={<i className="fas fa-heart" />} />
      </div>

      <div className="flex w-72 flex-col gap-4">
        <Input color="blue" label="Input Blue" />
        <Input color="purple" label="Input Purple" />
        <Input color="indigo" label="Input Indigo" />
        <Input color="teal" label="Input Teal" />
      </div>

      <div className="w-96">
        <Textarea label="Message" />
      </div>

      <div className="flex w-96 flex-col gap-4">
        <Textarea color="blue" label="Textarea Blue" />
        <Textarea color="purple" label="Textarea Purple" />
        <Textarea color="indigo" label="Textarea Indigo" />
        <Textarea color="teal" label="Textarea Teal" />
      </div>

      <div className="flex flex-col items-center">
        <h3 className="uppercase py-5 text-xl hover:underline underline-offset-8">Ajout de livres</h3>
        <label>Nom</label>
        <input type="text" className="form-input px-4 py-3 rounded-full" onChange={(e) => setName(e.target.value)} />
        <label>Auteur</label>
        <input type="text" className="form-input px-4 py-3 rounded-full" onChange={(e) => setAuthor(e.target.value)} />
        <label>Description</label>
        <input type="text" className="form-input px-4 py-3 rounded-full" onChange={(e) => setDescription(e.target.value)} />
        <select className="form-select px-4 py-3 rounded-full">       
        </select>
        <label>Images</label>
        <input type="url" className="form-input rounded-full" onChange={(e) => setImageUrl(e.target.value)} />
        <input type="checkbox" className="form-checkbox rounded text-pink-500" />
        <label>Dispo ?</label>
        <select className="form-multiselect px-4 py-3 rounded-full">
          
        </select>
        <input type="textarea"  className="form-input px-4 py-3 rounded-full" />
        <button onClick={postData} type="submit" className="bg-cyan-500 hover:bg-cyan-600 rounded-full px-4 py-3">ajouter</button>
      </div>
            
        {/*             <label>First Name</label>
                    <input placeholder='First Name' onChange={(e) => setFirstName(e.target.value)}/>
                
                    <label>Last Name</label>
                    <input placeholder='Last Name' onChange={(e) => setLastName(e.target.value)}/>
                
                    <Checkbox label='I agree to the Terms and Conditions' onChange={(e) => setCheckbox(!checkbox)}/>
                
                <Button type='submit'>Update</Button> */}
           
        </div>
    )
}