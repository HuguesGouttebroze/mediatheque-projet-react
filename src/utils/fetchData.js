import { useState, useEffect } from 'react'

export function FetchData() {
  const [users, setUsers] = useState([]);
   useEffect(() => {
    fetch("https://jsonplaceholder.typicode.com/users")
    .then((response) => response.json())
    .then((json) => setUsers(json));
  }, []); 

  const mapData = () => {
    let filterArray = users.filter((user) => {
    return user.name.includes("a") && user.id < 8;
  })
  console.log(filterArray);
  setUsers(filterArray);
  }

  return (
    <div className="App">
      <h3>Liste des adhérents en 2022 de la médiathèque:</h3>
      <div className="card">
        {users.map((user) => (
          <div>
            <p>{user.name}</p>
            <p>{user.userName}</p>
            
          </div>
        ))}
      </div>
      <button onClick={mapData}>"user"</button>
    </div>
  )
}
