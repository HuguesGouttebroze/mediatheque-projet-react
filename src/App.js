//import logo from './logo.svg';
import './App.css';
import { useState } from "react";
import Button from "./components/crud/Button";
import Formulaire from "./components/crud/FormMedia";
import { Media } from "../../Media";

function Upload() {
	const [file, setFile] = useState();
	function handleChange(e) {
		console.log(e.target.files);
		setFile(URL.createObjectURL(e.target.files[0]));

		return (
			<>
				<h3>Ajouter une image</h3>
				<input type="file" onChange={handleChange} />
				<img src={file} />
			</>
		)
	}

}
function App() {
	const [displayForm, setDisplayForm] = useState(false);
	const [medias, setMedias] = useState([]);

	const updateTasks = (media) => {
		setMedias((prevTasks) => [...prevTasks, setMedias]);
	};
	const removeTask = (index) => {
		setMedias((prevTasks) => prevTasks.filter((media, i) => i !== index));
	};
	const completeTask = (index) => {
		setMedias((prevTasks) => {
			return prevTasks.map((media, i) => {
				if (i !== index) {
					return media;
				} else {
					media.done = true;
					return media;
				}
			});
		});
	};
	return (
		<div className="App container">
			<Upload />
			<header>
				<h1>Todolist</h1>
				<Button
					label={
						displayForm
							? "Fermer le formulaire"
							: "Ajouter une tâche"
					}
					onClick={() => setDisplayForm(!displayForm)}
				/>
			</header>
			<Formulaire show={displayForm} addTask={updateTasks} />
			<ul className="tasks-list">
				{tasks.map((task, index) => (
					<li key={index}>
						<Task
							label={task.label}
							done={task.done}
							remove={removeTask}
							complete={completeTask}
							index={index}
						/>
					</li>
				))}
			</ul>
		</div>
	);
}

export default App;


