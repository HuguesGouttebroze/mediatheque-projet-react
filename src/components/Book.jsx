import React from 'react';
import { useHistory } from 'react-router-dom';

const Book = ({
  file,
  id,
  bookname,
  author,
  description,
  image,
  price,
  quantity,
  date,
  handleRemoveBook
}) => {
  const history = useHistory();

  return (
    <div className="container flex font-sans">
      <div className="flex-none w-48 relative">
        <img src={image} alt={bookname} className="absolute inset-0 w-full h-full object-cover" loading="lazy" />
        <img src={file} alt={bookname} className="absolute inset-0 w-full h-full object-cover" loading="lazy" />
        <p>{bookname}</p>
      </div>
      <form className="flex-auto p-6">
        <div className="flex flex-wrap">
          <h1 className="flex-auto text-lg font-semibold text-slate-900">
            {bookname}
          </h1>
          <div>
              Mis à jour: {new Date(date).toDateString()}
          </div>
          <div className="text-lg font-semibold text-slate-500">
            Autheur : {author}
          </div>
          <div className="line-clamp-3 md:line-clamp-6 text-lg font-semibold text-slate-500">
            Description : {description}
          </div>
          <div className="w-full flex-none text-sm font-medium text-slate-700 mt-2">
            Disponibilité:  OUI, disponible soit en écoute, au format mp3 sur notre applicatrion, 
            <br />
            soit en location (les livres & disques peuvent être commandé et livré par la poste, voir détail plus bas).
          </div>
        </div>
        <div className="flex items-baseline mt-4 mb-6 pb-6 border-b border-slate-200">
          <div className="space-x-2 flex text-sm">
            <label>
              <input className="sr-only peer" name="size" type="radio" value="xs" checked />
              <div className="w-9 h-9 rounded-lg flex items-center justify-center text-slate-700 peer-checked:font-semibold peer-checked:bg-slate-900 peer-checked:text-white">
                Poche
              </div>
            </label>
            <br />
            <label>
              <input className="sr-only peer" name="size" type="radio" value="s" />
              <div className="w-9 h-9 rounded-lg flex items-center justify-center text-slate-700 peer-checked:font-semibold peer-checked:bg-slate-900 peer-checked:text-white">
                Roman
              </div>
            </label>
            <br />
            <label>
              <input className="sr-only peer" name="size" type="radio" value="m" />
              <div className="w-9 h-9 rounded-lg flex items-center justify-center text-teal-700 peer-checked:font-semibold peer-checked:bg-slate-900 peer-checked:text-white">
                Audio
              </div>
            </label>
            <br />
            <label>
              <input className="sr-only peer" name="size" type="radio" value="l" />
              <div className="w-9 h-9 rounded-lg flex items-center justify-center text-orange-700 peer-checked:font-semibold peer-checked:bg-slate-900 peer-checked:text-white">
                En téléchargement
              </div>
            </label>
            <br />
            <label>
              <input className="sr-only peer" name="size" type="radio" value="xl" />
              <div className="w-9 h-9 rounded-lg flex items-center justify-center text-purple-700 peer-checked:font-semibold peer-checked:bg-slate-900 peer-checked:text-white">
                En streaming
              </div>
            </label>
          </div>
        </div>
        <div className="flex space-x-4 mb-6 text-sm font-medium">
          <div className="flex-auto flex space-x-4">
              <button className="h-10 px-6 bg-gray-900 text-gray-100 hover:bg-gray-100 hover:text-gray-900 font-semibold rounded-md shadow-xl" 
                type="submit"
                ripple={true} 
                onClick={() => history.push(`/edit/${id}`)} 
              >
                Mettre à Jour
              </button>
              <button className="h-10 px-6 font-semibold rounded-md border border-slate-200 bg-gray-100 text-gray-900 hover:bg-gray-900 hover:text-gray-100 shadow-xl" 
                type="button" 
                variant="text" 
                ripple={true} 
                onClick={() => handleRemoveBook(id)}
              >
                Supprimer
              </button>
          </div>
          <button className="flex-none flex items-center justify-center w-9 h-9 rounded-md text-slate-300 border border-slate-200" type="button" aria-label="Like">
            <svg width="20" height="20" fill="currentColor" aria-hidden="true">
              <path fill-rule="evenodd" clip-rule="evenodd" d="M3.172 5.172a4 4 0 015.656 0L10 6.343l1.172-1.171a4 4 0 115.656 5.656L10 17.657l-6.828-6.829a4 4 0 010-5.656z" />
            </svg>
          </button>
        </div>
      </form>
    </div>
  );
};

export default Book;