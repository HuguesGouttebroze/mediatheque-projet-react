import { useState, useEffect } from "react";
import { NavLink } from 'react-router-dom';
import {
  Navbar,
  MobileNav,
  Typography,
  IconButton,
} from "@material-tailwind/react";
 
export default function Nav() {
  const [openNav, setOpenNav] = useState(false);
 
  useEffect(() => {
    window.addEventListener(
      "resize",
      () => window.innerWidth >= 960 && setOpenNav(false)
    );
  }, []);
 
  const navList = (
    <ul className="text-center bg-lime-500/70 mb-4 mt-2 flex flex-col gap-2 lg:mb-0 lg:mt-0 lg:flex-row lg:items-center lg:gap-6">
      <Typography
        as="li"
        variant="small"
        color="blue-gray"
        className="p-1 font-normal bg-lime-700/50"
      >
        <NavLink to="/" className="text-center text-3xl p-4 link flex items-center" activeClassName="active" exact>
          Nos livres
        </NavLink>
      </Typography>

      <Typography
        as="li"
        variant="small"
        color="blue-gray"
        className="p-1 font-normal"
      >  
        <NavLink to="/add" className="text-center text-3xl link flex items-center" activeClassName="active">
          Ajouter un ouvrage
        </NavLink>   
      </Typography>   
    </ul>
  );
 
  return (
    <Navbar className="navbar mx-auto max-w-screen-xl py-2 px-4 lg:px-8 lg:py-4">
      <div className="container mx-auto flex items-center justify-between text-blue-gray-900">
        <h1 className="text-4xl text-center text-gray-100 px-4 w-30 mr-4 cursor-pointer py-1.5 font-normal flex flex-col items-center uppercase py-5 text-xl hover:underline underline-offset-8"> Médiathèque Numérique - Accès à la culture</h1> 
        <div className="hidden lg:block">{navList}</div>
        <IconButton
          variant="text"
          className="ml-auto h-6 w-6 text-inherit hover:bg-transparent focus:bg-transparent active:bg-transparent lg:hidden"
          ripple={false}
          onClick={() => setOpenNav(!openNav)}
        >
          {openNav ? (
            <svg
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              className="h-6 w-6"
              viewBox="0 0 24 24"
              stroke="red"
              strokeWidth={2}
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                d="M6 18L18 6M6 6l12 12"
              />
            </svg>
          ) : (
            <svg
              xmlns="http://www.w3.org/2000/svg"
              className="h-6 w-6"
              fill="none"
              stroke="pink"
              strokeWidth={2}
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                d="M4 6h16M4 12h16M4 18h16"
              />
            </svg>
          )}
        </IconButton>
      </div>
      <MobileNav open={openNav}>
        {navList}
        <button variant="gradient" size="sm" fullWidth className="bg-lime-500">
          <span>Say HELLO !</span>
        </button>
      </MobileNav>
    </Navbar>
  );
}