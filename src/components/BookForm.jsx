import React, { useState } from 'react';
import { Form } from 'react-bootstrap';
import { v4 as uuidv4 } from 'uuid';

const BookForm = (props) => {
  const [book, setBook] = useState(() => {
    return {
      bookname: props.book ? props.book.bookname : '',
      author: props.book ? props.book.author : '',
      description: props.book ? props.book.description : '',

      image: props.book ? props.book.image : '',

      quantity: props.book ? props.book.quantity : '',
      price: props.book ? props.book.price : '',
      date: props.book ? props.book.date : '',

      file: props.book ? props.book.file : ''
    };
  });

	const [file, setFile] = useState();

	function handleChange(e) {
		console.log(e.target.files);
		setFile(URL.createObjectURL(e.target.files[0]));
  };

  const [errorMsg, setErrorMsg] = useState('');
  const { bookname, author, description, image, price, quantity } = book;

  const handleOnSubmit = (event) => {
    event.preventDefault();
    // put fields required in the "values" array
    const values = [bookname, author, description];
    let errorMsg = '';

    const allFieldsFilled = values.every((field) => {
      const value = `${field}`.trim();
      return value !== '' && value !== '0';
    });

    if (allFieldsFilled) {
      const book = {
        id: uuidv4(),
        bookname,
        author,
        description,
        image,
        file,
        price,
        quantity,
        date: new Date()
      };
      props.handleOnSubmit(book);
    } else {
      errorMsg = 'Tous les champs sont requis, merci.';
    }
    setErrorMsg(errorMsg);
  };

  const handleInputChange = (event) => {
    const { name, value } = event.target;
    switch (name) {
      case 'quantity':
        if (value === '' || parseInt(value) === +value) {
          setBook((prevState) => ({
            ...prevState,
            [name]: value
          }));
        }
        break;
      case 'price':
        if (value === '' || value.match(/^\d{1,}(\.\d{0,2})?$/)) {
          setBook((prevState) => ({
            ...prevState,
            [name]: value
          }));
        }
        break;
      default:
        setBook((prevState) => ({
          ...prevState,
          [name]: value
        }));
    }
  };

  return (
    <div className="container">
      {errorMsg && <p className="errorMsg">{errorMsg}</p>}
      <h3>Ajout d'un livre de location - ADMIN - Gestion Médiathèque</h3>
      <Form onSubmit={handleOnSubmit}>
        <Form.Group controlId="name">
          <Form.Label>Titre du livre</Form.Label>
          <Form.Control
            className="input-control"
            type="text"
            name="bookname"
            value={bookname}
            placeholder="Entrer le nom du livre"
            onChange={handleInputChange}
          />
        </Form.Group>
        <Form.Group controlId="author">
          <Form.Label>Autheur</Form.Label>
          <Form.Control
            className="input-control"
            type="text"
            name="author"
            value={author}
            placeholder="Renseigner le nom de l'autheur"
            onChange={handleInputChange}
          />
        </Form.Group>
         <Form.Group controlId="description">
          <Form.Label>Description du livre</Form.Label>
          <Form.Control
            className="input-control"
            type="text"
            name="description"
            value={description}
            placeholder="Entrer la description du livre"
            onChange={handleInputChange}
          />
        </Form.Group>
        <Form.Group controlId="image">
          <Form.Label>
            <h3>
              IMAGE :
              <br />
              UPLOADER 
              ou 
              ENTRER une URL
            </h3>
          </Form.Label>
          <p>Renseigner une URL</p>
          <Form.Control
            className="input-control"
            type="url"
            name="image"
            value={image}
            placeholder="Renseigner l'url de l'image du livre"
            onChange={handleInputChange}
          />
          <p>
            OU
            <br />
            UPLOADER / TELECHARGER un IMAGE
          </p>
          <input type="file" onChange={handleChange} />
          <img src={file} alt="" />
        </Form.Group>
        <Form.Group controlId="quantity">
          <Form.Label>Quantité à mettre en location</Form.Label>
          <Form.Control
            className="input-control"
            type="number"
            name="quantity"
            value={quantity}
            placeholder="Entrer la quantité d'ouvrage souhaitée"
            onChange={handleInputChange}
          />
        </Form.Group>
        <Form.Group controlId="price">
          <Form.Label>Prix de l'ouvrage</Form.Label>
          <Form.Control
            className="input-control"
            type="text"
            name="price"
            value={price}
            placeholder="Renseigner le prix d'achat du document"
            onChange={handleInputChange}
          />
        </Form.Group>
        
        <button type="submit" className="shadow-xl h-14 w-34 rounded-lg bg-gradient-to-r from-cyan-400 to-blue-600">
          Ajouter
        </button>
      </Form>
    </div>
  );
};

export default BookForm;