# Implémentez une Architecture Orientée Services (SOA) 
## Réalisez des tests fonctionnels

## Comment tester une architecture SOA

## 4 types de TESTS

* Un SI conçu autour d'une architecture SOA nécessite des tests bien différents de ceux qu'on peut voir dans des architectures plus anciennes.

* En effet, le système étant composé de plusieurs services indépendants, il ne suffit pas que chacun fonctionne correctement en interne (tests unitaires), il faut qu'il prouve qu'il est capable de fonctionner parfaitement dans son environnement.

* Il faut donc réaliser les 4 types de tests suivants :

    1. Tests fonctionnels : 

        - test de fonctionnement des services : 
        
            + "recevez-vous les bonnes réponses ?" 
            + "Les bons formats ?" 
            + "Recevez-vous toutes les balises attendues ?" 
            ...etc...

    2. Tests de performances : 
    
        + nos services supportent-ils une montée en charge ? 
        + nos services répondent-ils assez vite ?

    3. Tests de sécurité : votre service refuse-t-il de répondre quand la stratégie de sécurité n'est pas respectée ? Que se passe-t-il quand on essaye de contourner celle-ci ?

    4. Tests d'intégration : vos services n'ont de sens que s'ils s'intègrent bien entre eux. Il faut donc tester des scénarios de combinaison de services, les protocoles de communication utilisés et la consistance des données qui transitent dans le système.

Beaucoup de développeurs se contentent de tester le système dans son intégralité. Par exemple, passer une commande dans une boutique créée en SOA. Ce genre de tests n'est absolument pas fiable. Il faut veiller à tester service par service, puis tester l'ensemble des services et leurs interactions grâce à des scénarios de tests.

Dans cette partie, nous allons nous concentrer sur les tests fonctionnels de nos services et voir comment SoapUI peut les faciliter et les automatiser.

## Qu'est-ce que SoapUI
    
    - SoapUI est un logiciel open source écrit en Java qui permet de tester les API qu'elles soient Soap ou RESTful.

    - SoapUI offre une panoplie de possibilités qui couvrent tous les tests nécessaires à une SOA.

    - Voici les principales fonctionnalités :

        + Test fonctionnels

        + Mocking : simulation de service

        + Tests de performances

        + Test de sécurité

        + Automatisations des tests

        + Intégration avec d'autres systèmes, comme ceux de l'intégration continue de type Jenkins.

    - SoapUI est populaire grâce à son interface qui permet de commencer à tester un service en quelques minutes et d'élaborer des scénarios complexes très simplement. Il est aussi tout à fait possible de créer des scripts avancés de tests grâce à Groovy.

    - Installer SoapUI et créer un premier projet

    - Suivez ensuite les instructions basiques pour l'installation du logiciel en gardant toutes les options par défaut.

Nous allons commencer par créer un projet vide. Cliquez sur l'icône SOAP en haut à gauche, puis renseignez LifeLeft en titre de votre projet et validez.

Nous allons tester le web service LifeLeft que nous avons créé dans la partie précédente. Assurez-vous que le service est bien déployé sur Glassfish et que celui-ci est en marche.

Retrouvez le lien vers le WSDl de ce service en vous rendant sur l'URL de celui-ci. Votre URL qui devrait alors ressembler à ceci : http://localhost:8080/lifeleft/LifeLeft?wsdl

Ajoutez le WSDl au projet : clic droit sur le nom du projet à gauche puis Add WSDL.

Figure 2
Figure 2
Ajoutez ensuite l'URL que vous avez récupéré et laissez les choix par défaut.

Figure 3
Figure 3
Vous obtenez alors une arborescence à gauche dans votre projet.

Figure 4
Figure 4
(1) : Le nom de votre projet que vous avez renseigné lors de la création sur SoapUI.

(2) : Le nom de la balise < binding > dans le WSDL dans laquelle sont déclarées les opérations possibles.

(3) : Liste des opérations possibles, dans ce cas il n'y qu'une anneesRestantesAVivre.

(4) : Une requête générée par SoapUI pour tester l'opération en question.

Vous pouvez double cliquer sur chaque opération ou projet pour avoir une vue détaillée sur les URL, les WSDL, etc.

Tester une opération
Pour tester une opération, il suffit d'utiliser la requête générée correspondante. Double cliquez sur Request 1 sous anneesRestantesAVivre.

Figure 5
Figure 5
Vous verrez à gauche la requête générée par SoapUI, vous reconnaîtrez la structure d'un message SOAP, avec Envelope et Body. Les trois arguments demandés par votre service sont représentés par arg0, arg1 et arg2.

Remplacez les " ? " par les arguments nécessaires puis cliquez sur play : bouton vert en haut à gauche.

Figure 6
Figure 6
La requête est alors envoyée à votre service sur le serveur Glassfish. Une réponse SOAP est générée par JAX-WS puis renvoyée vers SoapUI. Cette réponse est celle que vous voyez sur le panneau de droite.

Comprendre les concepts Test Suit, Test Case et Test Step
Afin de comprendre ces concepts, je vous invite à modifier notre service pour ajouter quelques méthodes.

package com.lifeleft;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.time.Year;
import java.util.concurrent.ThreadLocalRandom;


@WebService(serviceName = "LifeLeft")
public class LifeLeftService {

    private static final Integer ESPERANCE_VIE_HOMMES = 79;
    private static final Integer ESPERANCE_VIE_FEMMES = 85;

    String homme = "homme";
    String femme = "femme";

    Integer evDeReference = 0;

    @WebMethod
    public String  anneesRestantesAVivre (String prenom, String genre, Integer anneeNaissance) {

        if(sexe.equals(homme)) evDeReference = ESPERANCE_VIE_HOMMES;
        else evDeReference = ESPERANCE_VIE_FEMMES;

        //Remarque, en cas de problème, vous pouvez changer Year.now().getValue() par Calendar.getInstance().get(Calendar.YEAR)
        Integer anneeRestantes = evDeReference -(Year.now().getValue() - anneeNaissance );

        return "Bonjour " + prenom + ", il vous reste " + anneeRestantes + " ans à vivre, profitez-en au maximum !";
    }

    @WebMethod
    public int creerClient(String login, String password) {

        /*
        Insérer le client dans la base de données et retourner son ID unique
         */
        return ThreadLocalRandom.current().nextInt(100, 900);
    }

    @WebMethod
    public String commanderCompteARebours(Integer clientId){

        /*
        Insérer la commande dans la base de données et retourner un message de validation.
         */
        return "Merci ! Votre commande pour le client : "+ clientId +" de compteur de vie est validée";

    }

}

Ce service dans ce cas est utilisé dans un site qui vous permet de calculer le nombre d'années restant à vivre, puis vous propose de vous vendre un compte à rebours. Si vous souhaitez l'acheter, il faut créer un compte.

J'ai donc ajouté 2 méthodes simples :

creerClient : qui simule une méthode qui crée un client, l'ajoute à une BDD puis retourne l'ID unique de celui-ci.

commanderCompteARebours : qui simule la prise en compte de la commande et retourne un message de confirmation.

Regardons maintenant comment on organise les tests :

Test Step : dans SoapUI, un Test Step est une action qui peut être une requête comme celle qu'on a faite plus tôt, un transfert de résultat d'un Test Step précédent vers le suivant ou encore un appel à une base de données.

Test Case : un Test Case (terme que vous retrouverez en général dans le domaine des tests logiciels) est un ensemble d'entrées, de conditions et de variables auxquels le service ou l'application doit répondre d'une certaine manière précise et attendue. Dans SoapUI ces entrées, variables et conditions, ce sont les Test Steps. Une fois tous ces Tests Steps enchaînés avec succès, le résultat doit correspondre à une sortie précise qu'on attend du service. Par exemple on s'attend à ce que le service nous retourne une phrase qui contient le nombre d'années restantes à vivre, et non pas juste un chiffre.

Test Suite : Un Test Suite est composé d'un ou de plusieurs Test Case . Il vise à exécuter automatiquement plusieurs Test Steps afin de vérifier leur bon fonctionnement dans un scénario d'utilisation. Par exemple : on voudrait tester le scénario dans lequel un utilisateur commande dans notre service un compteur sans avoir calculé auparavant les années lui restant à vivre ou encore le scénario dans lequel après avoir calculé le nombre d'années, puis s'être enregistré, l'utilisateur du service passe commande en fournissant un ID client différent du sien.

Créer des Test Cases et Test Suites
Pour tester ces fonctionnalités, nous allons importer notre nouveau WSDL généré après la mise à jour de notre Class.

Mettez à jour la Class avec le code fourni précédemment.

Packagez puis uploadez sur Glassfish.

Récupérez le WSDL comme fait précédemment.

Supprimez l'ancien projet que nous avons créé : clic droit sur le projet > Remove.

Créez un nouveau projet et appelez le LifeLeft.

Ajoutez le nouveau WSDL comme fait précédemment.

Vous obtiendrez alors ceci :

Figure 7

Nous avons les 3 méthodes que nous avons définies dans notre Class avec, pour chacune, une requête générée.

Vous allez maintenant mettre creerClient et commanderCompteARebours dans un Test Case.

1- Faites un clic droit sur Request 1 sous commanderCompteARebours puis Add to TestCase.

Figure 8

1- Une fenêtre s'ouvre vous demandant de créer un Test Suite. En effet les Test Case dans SoapUI doivent être regroupés dans un Test Suite. Acceptez le nom par défaut "TestSuite 1".

Figure 9

1- C'est maintenant qu'on vous propose de créer le Test Case, nommez-le "TestCase Commande".

Figure 10

1- Une nouvelle fenêtre s'ouvre, vous proposant de donner un nom à cette requête que vous venez d'ajouter. Acceptez le nom par défaut.

Figure 11

1- Vous obtenez votre premier Test Suite avec un Test Case dedans qui est composé de Test Steps. Ici il n'y a qu'un Test Step correspondant à la requête vers notre opération commanderCompteARebours.

Figure 12

1- Pour ajouter la deuxième requête, faites un clic droit sur Request 1 sous creerClient puis Add to TestCase. Une fenêtre s'ouvre avec une liste déroulante vous proposant de choisir sous quel Test Suite et quel Test Case vous voulez ajouter la nouvelle requête. Choisissez celle que nous venons de créer puis validez.

Figure 13

Validez ensuite comme précédemment la fenêtre du choix du nom de cette requête. Vous obtenez cette arborescence :

Figure 14

Très bien ! Vous avez maintenant une Test Suite qui comporte un seul Test Case composé de 2 étapes.

1- Pour finir, nous allons faire exactement la même chose pour la dernière requête en la mettant dans sa propre Test Suite et Test Case. Cliquez droit sur Request 1 sous anneesRestantesAVivre puis Add to TestCase. Choisissez cette fois dans la liste déroulante Create new TestSuite.

Figure 15

Suivez ensuite les mêmes étapes que plus haut, afin d'obtenir cette arborescence :

Figure 16

Les propriétés dans SoapUI
Vous êtes presque prêts pour lancer vos tests… mais il y a un hic ! Si vous allez dans une des requête ajoutées et que vous double cliquez dessus pour voir le contenu, vous obtiendrez quelque chose comme ça :

<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:lif="http://lifeleft.com/">
   <soapenv:Header/>
   <soapenv:Body>
      <lif:commanderCompteARebours>
         <!--Optional:-->
         <arg0>30</arg0>
      </lif:commanderCompteARebours>
   </soapenv:Body>
</soapenv:Envelope>

agr0 aura soit une valeur que vous avez renseigné plus tôt (dans mon cas, c'est 30, mais vous pourriez avoir une autre valeur), soit une valeur par défaut "?". Dans tous les cas cette valeur est écrite en dur dans la requête. Or ceci est une mauvaise pratique, d'une part parce que si vous avez 70 requêtes à tester, vous n'allez pas pouvoir aller dans chacune pour changer la valeur selon les tests, et d'autre part parce que ça ne nous permet pas d'indiquer des paramètres arbitraires pour tester le comportement de votre service.

Les propriétés sont la solution idéale dans SoapUI. Vous pouvez les voir comme des variables que l'on définit à l'avance et qui seront injectées dans les requêtes que nous allons exécuter.

Les propriétés peuvent être définies à plusieurs niveaux. Le plus haut niveau étant celui du logciel en soit, un peu comme une variable globale.

Nous allons ici nous intéresser à ceux qu'on peut définir dans notre projet.

Si vous cliquez sur le nom du projet LeftLife, vous verrez en bas s'afficher 2 onglets avec Projet Properties et Custom Properties.

Figure 17

L'onglet Projet Properties représente les propriétés du projet comme son nom par exemple. L'onglet Custom Properties est celui dans lequel vous pouvez définir vos propres propriétés à utiliser plus tard.

Les mêmes onglets existent pour TestSuite 1 et TestCase 1. C'est dans ce dernier que nous allons définir nos variables.

Pourquoi autant d'endroits pour définir les propriétés, on ne peut pas juste les définir toutes au niveau du projet ?

Eh bien là encore il vous faut imaginer un cas réel où vous avez un logiciel dans lequel vous devez tester 12 services et 120 requêtes possibles par exemple. Dans ce cas, vous auriez des centaines de variables à gérer. Pour en retrouver une, c'est un peu comme chercher une aiguille dans une botte de foin avec des gants de boxeur ! :D

Vous allez définir les propriétés nécessaires au niveau de chaque Test Case.

1- Cliquez sur Test Case Commande puis en bas dans l'onglet Custom Properties cliquez sur le symbole " + " et créez les variables login et password.

Figure 18

2- Nous allons maintenant passer ces propriétés à notre requête creerClient sous Test Case Commande. Double cliquez sur celle-ci puis enlevez les valeurs par défaut dans le panneau de droite.

<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:lif="http://lifeleft.com/">
   <soapenv:Header/>
   <soapenv:Body>
      <lif:creerClient>
         <!--Optional:-->
         <arg0></arg0>
         <!--Optional:-->
         <arg1></arg1>
      </lif:creerClient>
   </soapenv:Body>
</soapenv:Envelope>

3- Faites maintenant un clic droit entre les balises et puis sélectionnez en bas Get Data puis TestCase Commande et enfin Property [login] que vous avez défini plus tôt.

Figure 19

Un identifiant unique de votre variable est ajouté :

${#TestCase#login}

4- Faites de même pour arg1 mais cette fois en choisissant password.

${#TestCase#login} ${#TestCase#password}

5- Cliquez sur le bouton vert "play" en haut à gauche pour lancer le test, votre service devrait répondre avec un chiffre généré aléatoirement comme prévu.

Comment pourrais-je savoir si ce sont bien mes variables qui sont passées effectivement à mon service ?

Pour voir ce qui se passe réellement avec nos variables, nous allons exécuter les tests dans notre Test Case.

6- Double cliquez sur Test Case Commande, une fenêtre s'ouvre avec nos deux requêtes.

Figure 20

7- Lancez les tests avec le bouton play en haut à gauche.

Figure 21

Une barre verte Finished en haut indique que les tests se sont déroulés avec succès.

8- En bas de la fenêtre, vous avez les étapes du test. Celle qui nous intéresse est la première creerClient - Request1, alors double cliquez dessus ! Vous obtenez une fenêtre qui présente la requête effectivement envoyée.

Figure 22

Bingo ! Vous voyez que vos propriétés ont été injectées dans le contenu des balises arg0 et arg1. Vous pouvez aussi voir la réponse grâce à l'onglet Respponse Message.

Félicitations, vous venez de compléter votre premier test. :) Néanmoins on a un problème : si vous revenez sur la fenêtre d'exécution du test Case et que vous double cliquez cette fois sur la 2e requête commanderCompteARebours, vous constaterez que la requête qui a été envoyée est celle-ci :

<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:lif="http://lifeleft.com/">
   <soapenv:Header/>
   <soapenv:Body>
      <lif:commanderCompteARebours>
         <!--Optional:-->
         <arg0>?</arg0>
      </lif:commanderCompteARebours>
   </soapenv:Body>
</soapenv:Envelope>

Aucun argument valide n'a été envoyé via cette requête. Or, pour cette requête, ne nous pouvons pas juste établir une variable pour tester, nous devons tester en lui passant l'identifiant client que nous a retourné la requête précédente.

Je vais vous expliquer comment transférer des propriétés.

Transférer des propriétés
Comme je l'ai dit précédemment, nous avons besoin de passer le résultat de la requête creerClient comme paramètre à la requête commanderCompteARebours afin que celle-ci enregistre la commande pour le bon client. Vous ne voulez pas envoyer un compte à rebours de vie à une mamie de 90 ans hein ? :lol:

Afin de transférer le résultat de la première requête vers la seconde qui sera envoyée à commanderCompteARebours, nous allons créer une propriété vide au même titre que login et password puis nous allons l'alimenter avec le résultat de la première requête.

1- Créez une variable clientId au niveau de TestCase Commande qui viendra s'ajouter aux deux autres.

2- Faites un clic droit sur TestCase Commande puis cliquez sur Add Step puis Preperty Transfer.

Figure 23

3- Nommez-la dans la boite de dialogue suivante : "Transfert ClientId".

4- Une nouvelle fenêtre s'ouvre. Si ce n'est pas le cas, double cliquez sur Property Transfer.

5- Ajoutez un nouveau transfert grâce au bouton "+" en haut à gauche et nommez-le clientId.

Figure 24

Fournir la source

À la droite de la fenêtre, vous avez deux blocs : un Source et un autre Target. Dans le bloc Source, nous allons définir la provenance de notre variable à transférer. Choisissez comme source creerClient - Request 1 puis dans le champs Property choisissez Response. Ce champ définit si l'on souhaite transférer une variable depuis la requête elle-même ou plutôt sa réponse voire d'autres sources comme le domaine, etc. Dans notre cas, c'est la réponse qui nous intéresse.

Figure 25

Le XPath

Ici les choses se corsent pour ceux qui ne maîtrisent pas bien le XML. En effet, nous allons devoir déterminer ici un XPath vers la balise dans laquelle se situe la valeur à transférer.

Voici un exemple de réponse qu'on obtient de creeClient .

<S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/">
   <S:Body>
      <ns2:creerClientResponse xmlns:ns2="http://lifeleft.com/">
         <return>765</return>
      </ns2:creerClientResponse>
   </S:Body>
</S:Envelope>

Il faut voir le XPath un peu comme la barre d'arborescence qu'on retrouve souvent dans les sites web.

Accueil > Vêtements > Hommes > Vestes > Vestes en cuir

Par analogie, le chemin vers la balise qui nous intéresse serait :

Envelope > Body > creerClientResponse > return

Sauf que voilà… il nous faut indiquer des espaces de nom devant chaque balise afin d'éviter toute confusion.

Afin d'intégrer ces namespaces, nous allons utiliser le bouton ns: dans notre fenêtre plutôt que de les copier.

Figure 26

Un message d'erreur va apparaître indiquant qu'il n'y a pas encore de Target. Ignorez-le, tout simplement !

Dans le champ sous Source vous obtiendrez tous les namespaces déclarés dans la réponse.

declare namespace soap='http://schemas.xmlsoap.org/soap/envelope/';
declare namespace ns1='http://lifeleft.com/';

À partir de là, il suffit de combiner notre arborescence avec les namespaces déclarés, cela donne

declare namespace S='http://schemas.xmlsoap.org/soap/envelope/';
declare namespace ns2='http://lifeleft.com/';

/S:Envelope/S:Body/ns2:creerClientResponse/return/text()

Devant chaque balise, il y a le préfixe déclaré pour l'espace de nom puis on finit par text() qui indique que nous souhaitons récupérer le contenu de la balise.

Bien sûr, dans ce cas c'est très simple, mais quand vous avez une réponse SOAP plus grande, cela va être beaucoup plus compliqué.

Je vous suggère alors une autre méthode.

Créez un fichier test.xml dans le projet LifeLeft sur IntelliJ. Celui-ci a une fonction qui permet de récupérer le XPath très facilement.

Faites simplement un clic droit sur le contenu :

Figure 27

Une pop-up s'affiche avec le XPath : cliquez sur l'icône copier à droite. Vous obtiendrez alors :

/S:Envelope/S:Body/ns2:creerClientResponse/return/text()

Veillez à changer les préfixes (S et ns2) en fonction des namespaces que nous avons importés plus tôt avec le bouton ns:.

De retour dans notre champ source, on obtient ceci :

declare namespace S='http://schemas.xmlsoap.org/soap/envelope/';
declare namespace ns2='http://lifeleft.com/';

/S:Envelope/S:Body/ns2:creerClientResponse/return/text()

Fournir la Target

Nous allons maintenant déterminer vers où nous souhaitons transférer cette valeur qu'on vient de cibler.

Vous vous souvenez que nous avons défini une propriété clientId au niveau de TestCase Commande. Nous allons diriger cette variable ciblée vers TestCase Commande.

Dans le champ Target choisissez comme cible TestCase Commande. Dans Property vous devriez retrouver clientId précédemment défini.

Figure 28

Tester le transfert

Pour tester si votre transfert fonctionne correctement, il suffit d'appuyer sur le bouton play en haut à gauche. Vous aurez alors en bas une réponse de ce type :

Figure 29

Maintenant, la colonne Transfer Name reprend le nom du transfert que nous avons défini au début dans le panneau de gauche, et dans la colonne Transfer Values, vous retrouvez la valeur qui lui sera attribuée.

Si vous avez une erreur dans cette dernière colonne, c'est que le plus souvent, votre XPath n'est pas correct. Si vous l'avez écrit à la main, alors je vous recommande d'utiliser la méthode IntelliJ.

Fermez ensuite la fenêtre directement, il n'y a pas de validation à faire.

Tester le transfert dans le TestCase

Vous devriez à ce stade avoir cette arborescence dans votre TestCase Commande.

Figure 30

:informationsource: Si votre étape _Property Transfert est en bas de la liste je vous recommande de la remonter entre les deux étapes précédentes afin que ce soit visuellement plus parlant. On récupère le résultat de la requête en haut pour le transférer vers celle d'en bas.

Nous allons donc maintenant indiquer dans la requête commanderCompteARebours que nous souhaitons qu'elle récupère comme paramètre la propriété clientId.

Double cliquez sur commanderCompteARebours puis comme on l'a fait pour creerClient, puis faites un clic droit sur le contenu de la balise arg0 et indiquez la propriété clientId.

Figure 31

Vous obtenez :

<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:lif="http://lifeleft.com/">
   <soapenv:Header/>
   <soapenv:Body>
      <lif:commanderCompteARebours>
         <!--Optional:-->
         <arg0>${#TestCase#cliendId}</arg0>
      </lif:commanderCompteARebours>
   </soapenv:Body>
</soapenv:Envelope>

Très bien, il ne reste plus qu'à lancer le Test Case. Cliquez sur le bouton play, en bas vous devriez avoir cette fois-ci 3 étapes. Si vous ouvrez l'étape 1 et 3 côte à côte, vous verrez que la réponse reçue par creerClient a été bien transférée à la requête commanderCompteARebours.

Figure 32
Figure 32 
Scripting
Tout ça est bien joli, mais je ne teste rien si c'est pour que j'aille changer la valeur des propriétés manuellement à chaque fois pour tester avec différente valeurs.

Effectivement quand on veut tester une méthode, une Class ou un service, il est inutile de la tester avec une seule valeur, il faut tester en fournissant des données aléatoires et voir si la fonctionnalité choisie réagit comme prévu.

Dans SoapUI Pro, il y a la possibilité d'ajouter une étape "DataGen" au même titre que Property Tranfer et qui générera des données pour vous automatiquement.

Comme SoapUI Pro est payant et que vous aimez tout ce qui est gratuit ;) nous allons voir comment générer ces données facilement grâce à une des fonctionnalités les plus puissantes de SoapUI : les Groovy Scripts.

Pour ceux d'entre vous qui ne le connaissent pas, Groovy est un langage de programmation qui reprend en grande partie la syntaxe de Java et qui permet dans SoapUI (mais pas que) de créer de petits scripts facilement.

Objectif

Nous voulons que les propriétés login et password soit alimentées avec des données aléatoires afin de voir si notre opération creerClient répond comme prévu.

Créer le script

Nous allons commencer par ajouter une étape à notre TestCase Commande. Cliquez sur Add Step puis sélectionnez Groovy Script, appelez-le : ParamAleatoires. Glissez-le en haut de la liste.

Figure 33

Double cliquez sur cette nouvelle étape ajoutée. Vous devriez obtenir une fenêtre comme celle-ci :

Figure 34

Nous allons commencer par nous familiariser avec cet outil en récupérant la valeur des propriétés que nous avons définit plus tôt.

Entrez ce code dans l'éditeur :

String login = testRunner.testCase.getPropertyValue( "login");
Oui on dirait du pur Java, et c'est le cas ! Techniquement, c'est un autre langage, mais dans la pratique vous allez utiliser les bibliothèques Java classiques ainsi que sa syntaxe. Groovy vient ajouter d'autres fonctionnalités, qui ne nous seront pas utiles ici.

Cette ligne récupère la propriété login grâce à testRunner. Celui-ci est votre ami, car c'est l'objet qui exécute les TestCase, il réitère sur les TestSteps et les exécute l'une après l'autre. Il vous permet donc l'accès à l'ensemble des propriétés et étapes qui entrent dans votre TestCase.

Cliquez sur play et une pop-up apparaîtra avec la valeur que vous avez donné précédemment à login.

À l'instar de getPropertyValue, setPropertyValue vous permet de changer la valeur d'une propriété.

Voici le script final pour fournir des données aléatoires à login et password :

import org.apache.commons.lang.RandomStringUtils;
import java.util.concurrent.ThreadLocalRandom;

int longueurParDefaut = 15
String charset = (('a'..'z') + ('A'..'Z') + ('0'..'9')).join()
String loginAleatoire = RandomStringUtils.random(longueurParDefaut, charset.toCharArray());

testRunner.testCase.setPropertyValue( "login", loginAleatoire);

int mdpAleatoire = ThreadLocalRandom.current().nextInt(1000, 9000);

testRunner.testCase.setPropertyValue("password", "${mdpAleatoire}");
Ligne 1 et 2 : on importe la bibliothèque RandomStringUtils qui permet de générer aléatoirement une chaîne de caractères à partir d'une autre. On importe également ThreadLocalRandom qui nous permettra comme dans la Class LifeLeft de générer des int aléatoires.

Ligne 4 : on définit la longueur des chaînes que nous souhaitons générer.

Ligne 5 : on génère une chaîne de caractères comportant tous les caractères que l'on souhaite utiliser dans la génération de notre chaîne aléatoire.

Ligne 6 : on fournit la longueur souhaitée et les caractères à mélanger à la méthode random de RandomStringUtils, que nous avons importé précédemment.

Ligne 8 : on utilise setPropertyValue pour remplacer la valeur de login par celle qu'on vient de générer.

Ligne 10 : on génère un int aléatoire.

Ligne 12 : on l'affecte à la propriété password. À noter qu'ici nous passons mdpAleatoire sous cette forme "${mdpAleatoire}" car setPropertyValue accepte uniquement des String comme argument.

Très bien, vous pouvez maintenant appuyer sur play. Rien ne se passe à l'écran, mais si vous cliquez sur TestCase Commande puis Custom Properties en bas, vous verrez que les valeurs changent à chaque fois que vous appuyez sur play.

Figure 35

Tester

Il ne vous reste plus qu'à double cliquer sur votre Test Case et appuyer sur play pour lancer le test.

Figure 36

Si vous double cliquez sur la requête en bas creerClient (Step 2 dans l'image ci-dessus), vous verrez que le login et mdp sont effectivement aléatoires.

Bien entendu, si vous voulez tester vraiment votre service, vous allez ajouter dans les chaînes aléatoires des caractères spéciaux, des espaces et tout un tas de bestioles pour voir si celui-ci répond correctement.

Le composant Groovy Script vous offre énormément de possibilitéS, par exemple la possibilité de faire tourner les étapes d'un Test Case en boucle autant de fois que vous le souhaitez. Je vous invite à consulter la documentation à ce sujet.

Assertions
Votre test fonctionne, mais vous n'allez tout de même pas aller vérifier par vous-même à chaque fois si le résultat correspond à ce que vous attendez ? Imaginez que la réponse soit un message SOAP de 1300 lignes, je crois que vous aurez perdu 2 points d'acuité visuelle avant de l'avoir validé à l'oeil nu ! :D

SoapUI offre la possibilité de valider automatiquement les réponses aux différentes requêtes, on appelle ça des Assertions.

Pour chaque requête ou Test Step, vous pouvez ajouter 0 ou plusieurs assertions afin de valider la réponse. Sans que vous le sachiez, SoapUI avait déjà placé une assertion par défaut pour valider les réponses reçues (et oui ça sert à quoi un test si on ne le valide pas).

Double cliquez sur la requête creerClient puis cliquez en bas sur Assertions.

Figure 37

Le panneau des assertions apparaît avec une assertion SOAP Response déjà présente. Celle-ci vérifie tout simplement si le message reçu est un message SOAP valide.

Pour ajouter une assertion, cliquez sur le bouton vert " + " en haut à gauche de ce panneau. Vous obtenez alors une liste des différents types d'assertions à appliquer.

Figure 38

Vous avez à gauche la catégorie des assertions et à droite les assertions utilisables dans notre requête.

Cliquez sur "Property Cotent". Parmi les plus utiles dans cette catégorie on retrouve :

Contains : permet de chercher dans l'ensemble de la réponse une valeur en particulier, on peut bien entendu utiliser des expressions régulières.

Not Contains : l'inverse de la précédente .

XPath : vous vous souvenez quand on a ciblé le contenu d'une balise grâce à un XPath ? Eh bien celui-ci peut vous servir encore ici, car vous pouvez avec cette assertion cibler le contenu d'une balise pour vérifier s'il correspond à une valeur que vous déterminez.

Cliquez à présent sur Compliance, Status and standards. Les fonctionnalités les plus utiles sont :

SOAP Fault : détermine si le message SOAP reçu ne comporte pas la balise fault qui indique qu'une erreur a été retournée (nous verrons cette balise dans le chapitre de gestion des erreurs).

Schema Compliance : celle-ci est extrêmement utile car elle permet de vérifier que la réponse obtenue correspond bien à ce qui a été prévu dans le WSDL. Cela implique la vérification des type de donnés : int ? String ? entre autres.

SOAP Response : très utile également, elle est incluse comme assertion par défaut (comme vu plus haut) mais vous pouvez la supprimer.

Enfin la catégorie Script permet de valider le XML de la réponse manuellement grâce encore une fois à Groovy.

Nous allons commencer par tester la balise Schema Compliance. Une fois sélectionnée, cliquez sur Add.

Une fenêtre va vous demander l'adresse du WSDL contre lequel on va valider la réponse. Normalement il est pré-rempli avec celui que vous avez indiqué à la création du projet.

Figure 39

Cliquez sur OK puis appuyez sur play pour lancer la requête. Vous ne devriez pas recevoir d'erreur car notre réponse est valide.

Figure 40

Afin de tester si notre assertion vérifie bien que dans notre balise < return > on a un int , remplacez la valeur de celle-ci manuellement dans la réponse par un String comme "test".

Afin de lancer cette assertion sans relancer la requête (car sinon on recevra de notre serveur une réponse valide), double cliquez tout simplement en bas dans le panneau des assertions sur Schema Compliance et validez le WSDL proposé.

Figure 41

Une vilaine erreur apparaît en bas nous indiquant que le mot test ne ressemble pas des masses à un chiffre décimal. o_O

Scripting

L'assertion la plus puissante est celle qui permet d'utiliser un script. Nous allons donc ici créer un script Groovy, pour vérifier si l'id client reçu est toujours inférieur à 1000 comme prévu.

Ajoutez une nouvelle assertion de type script.

Figure 42

Une fenêtre semblable à ce qu'on a vu plus tôt avec les propriétés aléatoires apparaît.

Voici le code qui nous permettra d'accomplir cela :

def util = new com.eviware.soapui.support.GroovyUtils(context);
def xml = util.getXmlHolder(messageExchange.responseContent);

xml.namespaces["S"]="http://schemas.xmlsoap.org/soap/envelope/";
xml.namespaces["ns2"] = "http://lifeleft.com/"

String clientId = xml.getNodeValue("/S:Envelope/S:Body/ns2:creerClientResponse/return");

if(clientId != null && !clientId.isEmpty()) {
    Integer idClientInt = clientId.toInteger();
    assert idClientInt < 1000;
}
else {
    assert false
}

def : dans Groovy, def sert à définir une variable sans type (eh oui, du Java sans le typage). D'ailleurs le def n'est pas obligatoire , il est surtout là pour limiter la portée de la variable à ce script uniquement.

Ligne 1 : on récupère une instance de GroovyUtils, qui est une Class fournie par SoapUI et qui permet d'avoir accès à quelques fonctions utiles comme le XmlHolder qui nous permet de manipuler et naviguer dans les réponses XML.

Ligne 2 : on récupère une instance de XmlHolder afin de l'utiliser pour retrouver la valeur que nous cherchons dans la réponse.

Ligne 4 et 5 : on définit les espaces de noms exactement comme on l'a fait pour les propriétés aléatoires.

Ligne 7 : on utilise la méthode getNodeValue de XmlHolder pour accéder à la valeur de la balise < return > exactement de la même manière que précédemment, à l'exception qu'ici on n'a pas besoin de finir par text() car getNodeValue récupère justement le contenu de la balise ciblée.

Ligne 9 : on vérifie que l'expression clientId n'est pas égale à null et n'est pas vide.

Ligne 10 : les valeurs récupérées dans un XML sont toujours des String, il faut donc convertir dans notre cas vers un Integer.

Ligne 11 : on utilise assert qui permet dans Groovy d'évaluer une expression et d'indiquer si elle est vraie ou fausse. Si la clientId est supérieure à 1000 alors SoapUI comprendra le retour d'assert et affichera que le test a échoué.

Ligne 14 : on renvoie un false si clientId est vide. Normalement, on ne devrait pas le faire ici, mais plutôt directement dans le WSDL en indiquant que cette balise ne doit pas être vide. Néanmoins pour l'exemple, nous renvoyons ici une erreur dans ce cas.

Très bien, pour tester ce script, appuyez sur le bouton play.

Figure 43

Vous devriez avoir un message de succès. Afin de tester si notre script marche vraiment, appuyez sur OK pour valider puis fermez cette fenêtre. Remplacez ensuite manuellement la valeur de la balise < return > par un chiffre supérieur à 1000.

Double cliquez à nouveau sur le script d'assertion dans le panneau du bas et appuyez sur le bouton play dans la fenêtre de l'éditeur de scripts.

Vous obtenez alors une erreur, comme prévu !

Figure 44

Vous pouvez faire le même test avec une balise < return > vide ou encore en changeant dans la Class du service la valeur clientId à retourner.

#
J'ai terminé ce chapitre et je passe au suivant

# Création de services répondant aux principes de la SOA		

# Simulez un service avec les mocks	

# Testez votre service grâce à SoapUI

# Réalisez des tests fonctionnels

# Gérez les erreurs

# Tests de services et gestion des erreurs