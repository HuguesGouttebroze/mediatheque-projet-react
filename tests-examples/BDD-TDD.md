# Pratiquer & Coder avec le BDD et le TDD - Behavior Driven Development - Test Driven Development 

Les tests peuvent être utiles à votre équipe de développement voire à votre entreprise toute entière si vous choisissez de suivre une méthode appelée le "behavior-driven development" (BDD).

Imaginez que vous êtes développeur(se), et que vous recevez une requête d'un chef de projet, un marketer, un cofondateur, ou de n'importe quelle autre personne dans votre entreprise. Vous avez besoin de traduire sa requête en code.  💬🔜💻

Penser en amont à la façon dont vous allez structurer et écrire vos tests va vous aider à écrire votre code par la suite.

BDD : "Behavior Driven Development"
Pour faire du BDD, on a besoin d'un but pour commencer. Utilisons une petite métaphore… : Avant de commencer à naviguer quelque part, il vaut mieux savoir où vous allez, n'est-ce pas ?

Et bien, c'est pareil avec le code. Imaginez que vous êtes le bateau représenté sur la carte ci-dessous, et que votre nouvelle fonctionnalité c'est la croix :❌

Let your tests get your feature from point A to point B!
Let tests get your feature from point A to point B!
Le behavior-driven development vous aidera à identifier exactement où votre code doit vous amener. 

Première étape : Collaborez et définissez
Quelqu'un dans votre entreprise a demandé le changement que vous êtes censé coder. Prenez le temps de discuter avec cette personne, de comprendre la vision qu'elle a de la future fonctionnalité et de confirmer qu'il y a un vrai besoin pour le changement proposé.

Par conséquent, la première étape de la pratique du "behavior-driven development" c'est de collaborer avec les autres personnes impliquées dans votre projet.

Grâce à cette collaboration, vous pouvez évaluer si le changement proposé est vraiment nécessaire et trouver le moyen le plus simple et clair de le faire.

Le BDD est particulièrement intéressant dans le développement frontend. En l'utilisant, vous pouvez considérer chaque nuance de l'interaction que quelqu'un aura avec votre site (et ce qu'il doit se passer ou pas).

Définissez des choses à tester
Imaginons le scénario suivant : vous travaillez pour un site d'information tech (par exemple Mashable, TechCrunch, etc), et un chef de projet veut que vous ajoutiez une fonctionnalité de commentaires pour que les gens puissent laisser leurs avis sur des articles. C'est tout ce que vous avez comme instructions. 😳

Ces instructions peuvent être suffisantes pour commencer, mais une fois que vous commencez à coder, vous allez voir qu'il y a beaucoup de choses à préciser sur la façon dont la fonctionnalité doit se comporter.

Vous allez vous rendre compte que vous ne savez pas :

la taille limite de chaque commentaire. 

ce qui doit se passer s'il y a des gros mots dans un commentaire. 

ce qui se passe si quelqu'un laisse une commentaire vide.

si n'importe qui peut laisser des commentaires ou seulement des gens qui ont des comptes.

etc.

C'est bien de trouver les réponses à ces questions avec l'équipe autour du projet, au lieu de rester seul dans son coin !

Pour rendre ce processus de collaboration plus facile, pensez à utiliser la syntaxe BDD standardisée qui est proposée ci-dessous.

La syntaxe BDD
Le BDD est basé sur des mots que l'on utilise souvent dans la vraie vie comme Étant donné, Quand, Alors, et Et, qui vont décrire le comportement de votre fonctionnalité.

Vous allez souvent voir ces mots en anglais :

Étant donné : Given

Quand : When

Alors : Then

Et : And

Vous décrivez une situation théorique et ce qui doit se passer en utilisant ces mots.

Par exemple :

Étant donné qu'un utilisateur laisse un commentaire

Quand le commentaire dépasse 1000 caractères

Alors le commentaire ne doit pas être sauvegardé

Et l'utilisateur doit voir un message d'erreur

Vous pouvez vous demander, mais où est le code ? Il n'y en a pas pour l'instant !

Certains frameworks de test vous permettent même d'utiliser ces mots-clés ("given", "when", "then", "and" en anglais) dans votre tests, et les rendent très lisibles.

Pratiquez des exemples de BDD
Nous allons parcourir quelques exemples ensemble pour voir ce que la pratique de BDD peut donner dans des scénarios différents.

Voir ces scénarios peut vous aider à mieux maitriser la syntaxe et la nature des spécifications behavior-driven.

La localisation
Les sites web ont souvent plusieurs versions disponibles pour des gens basés dans des pays différents et qui parlent des langues différentes. Un utilisateur doit pouvoir voir la version correcte qui correspond à son pays ou à sa langue.  🌍

Par exemple, un utilisateur en Espagne doit voir la page d'accueil en espagnol, n'est-ce pas ? Ça serait un peu absurde de leur montrer la version chinoise du site. Même s'il y a peut être des gens en Espagne qui parlent chinois, il est plus probable qu'ils parlent espagnol.

En utilisant les mots clés given, when, and then (et and si nécessaire), voici comment vous pouvez écrire ces scénarios différents (on fait l'exemple en anglais parce qu'il y a beaucoup d'équipes de développement qui travaillent en anglais) :

Given a user is in Spain

When they visit the homepage

Then they should see the Spanish version of the homepage

Vous pourrez écrire le même genre de spécification pour les utilisateurs en France :

Given a user is in France

When they visit the homepage

Then they should see the French version of the homepage

Pensez à écrire des cas négatifs aussi (ce qui ne doit pas se passer !):

Given a user is in Spain

When they visit the homepage

Then they should not see the French version of the homepage

En détaillant des spécifications comme ca, vous rendrez plus clairs les scénarios que vous aurez besoin de considérer dans vos tests de code.

Morpion
Disons que vous avez un jeu en ligne qui permet aux gens de jouer au morpion ensemble.. ❌⭕️

Les carrés sont arrangés comme suit :

1

2

3

4

5

6

7

8

9

Vous ne voulez pas que deux joueurs puissent occuper le même carré à la fois, parce qu'en morpion, chaque carré ne peut contenir qu'un seul joueur.

Un test qui confirme qu'un joueur ne doit pas pouvoir prendre un carré quand il est déjà occupé par l'autre joueur serait pertinent. Ici un exemple du comportement cherché : 

Given player 1 is on square 3

When player 2 attempts to take square 3

Then player 2 should receive an error

And they should not be able to take square 3

Si vous utilisez des rôles ou des cas précis, ex. "player 1" et "square 3" (au lieu de "a player" ou "a square"), ce sera beaucoup plus facile d'écrire les tests correspondants plus tard !

Maintenant que vous avez vu quelques exemples d'écritures de spécifications "behavior-driven," vous verrez dans le chapitre suivant comment les utiliser dans un processus de "test-driven development".

#
J'ai terminé ce chapitre et je passe au suivant
Pourquoi écrire des tests ?		Apprenez le test driven development (TDD)	