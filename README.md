# Getting Started with Create React App

## Update add upload image feature on add form

+ Last commit (""), we add an upload feature on our add book form.

+ To give the choise beetween upload an image or fill an url image, in the add form field, we have also change the required files, now only 3 fields are required, title, author and description.

## Docker Compose

+ We add a `docker-compose.yml` file.

+ Run the following cmd :

## TODO

+ Add a feature that require to choose one of the 2 images fields ...
+ Style books form & the book card ...
+ Workin's on Docker file, specialy on Docker Compose features.

-----------------

+ See code repo on Gitlab [here](https://gitlab.com/HuguesGouttebroze/mediatheque-projet-react)

+ This `React` application have been deployed [here](https://mediatheque-projet-react.vercel.app/)

+ We are using `Vercel` services to deployed in continious. 

+ The application is refresh after every commit that we push on our `Gitlab`'s repository. 

+ We are using the versionning tools, `git`.

+ We need `Node.js` & one of it's package installer, like `npm`, `yarn`, or again `pnpm`.

+ Here, we'll using `npm`.


### Create React App

+ To create a `Create React Application`, run the following snippets:

    + `npx create-react-app my-project`

    + `npm install`

    + `cd ...`

    + `npm start`

### Dependencies


* We will install the following paquetges:

    + `npm i react-router-dom@5` 
    + `npm i uuid` 
    + `npm i lodash` 
    + `npm i react-bootstrap`
    + `npm i @material-tailwind/react`
    + `npm i -D tailwindcss postcss autoprefixer`
    + `npx tailwindcss init -p`
    + `npm i axios`

+ See more information [here](https://tailwindcss.com/docs/guides/create-react-app) for `TailwinCSS` installation & configuration.

+ add `tailwind`'s plugins :
  `npm install -D @tailwindcss/line-clamp`

  - We'll using the `line-clamp` utilities to specify how many lines of text should be visible before truncating

  - we will using this to trincated the description of our Card book component
    - on small screen screen, we want 5 lines & 3 lines on small sceens before clamp.

```jsx
<p class="line-clamp-3 md:line-clamp-5">
  Et molestiae hic earum repellat aliquid est doloribus delectus. Enim illum odio ...
</p>
```
### Tailwind CSS Optimisation for production

+ This design system aims to produce the smallest CSS file possible by only generating the CSS we are actually using.

+ Combined with minification and network compression

+ See [Optimizing for Production](https://tailwindcss.com/docs/optimizing-for-production) into `taimwind`'s documentation

### Using React with Docker

1. Set Up Docker
If `Docker` isn't install, we need to do it.  

We can install `Docker desktop`.

Once it's installing, we will run the following command to verify `Docker` & `Docker compose` status & version.

  `docker -v`
Docker version 20.10.20

Next, we will create a file called `Dockerfile` at the root of our project

# pull the base image
FROM node:alpine

# set the working direction
WORKDIR /app

# add `/app/node_modules/.bin` to $PATH
ENV PATH /app/node_modules/.bin:$PATH

# install app dependencies
COPY package.json ./

COPY package-lock.json ./

RUN npm install

# add app
COPY . ./

# start app
CMD ["npm", "start"`]

+ We'll create a `.dockerignore` file. This will help us ignore `node_modules`, `.env` etc 
```
**/node_modules
**/npm-debug.log
```

+ build an image upon which your container will be built.

`$ docker build -t ps-container:dev .`

+ next 
`
$ docker run -it --rm \
-v ${PWD}:/app \
-v /app/node_modules \
-p 3001:3000 \
-e CHOKIDAR_USEPOLLING=true \
ps-container:dev`


### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can't go back!**

If you aren't satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you're on your own.

You don't have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn't feel obligated to use this feature. However we understand that this tool wouldn't be useful if you couldn't customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

### Code Splitting

This section has moved here: [https://facebook.github.io/create-react-app/docs/code-splitting](https://facebook.github.io/create-react-app/docs/code-splitting)

### Analyzing the Bundle Size

This section has moved here: [https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size](https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size)

### Making a Progressive Web App

This section has moved here: [https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app](https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app)

### Advanced Configuration

This section has moved here: [https://facebook.github.io/create-react-app/docs/advanced-configuration](https://facebook.github.io/create-react-app/docs/advanced-configuration)

### Deployment

This section has moved here: [https://facebook.github.io/create-react-app/docs/deployment](https://facebook.github.io/create-react-app/docs/deployment)

### `npm run build` fails to minify

This section has moved here: [https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify](https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify)
